package com.example.recipe.mappers;

import com.example.recipe.dtos.IngredientDto;
import com.example.recipe.model.Ingredient;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class IngredientToIngredientDto implements Converter<Ingredient, IngredientDto> {

    private final UnitOfMeasureToUnitOfMeasureDto uomConverter;

    @Autowired
    public IngredientToIngredientDto(UnitOfMeasureToUnitOfMeasureDto uomConverter) {
        this.uomConverter = uomConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public IngredientDto convert(Ingredient source) {
        if (source == null) {
            return null;
        }

        IngredientDto ingredientDto = new IngredientDto();
        ingredientDto.setId(source.getId());

        if (source.getRecipe() != null) {
            ingredientDto.setRecipeId(source.getRecipe().getId());
        }

        ingredientDto.setAmount(source.getAmount());
        ingredientDto.setDescription(source.getDescription());
        ingredientDto.setUnitOfMeasure(uomConverter.convert(source.getUnitOfMeasure()));
        return ingredientDto;
    }
}
