package com.example.recipe.mappers;

import com.example.recipe.dtos.UnitOfMeasureDto;
import com.example.recipe.model.UnitOfMeasure;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UnitOfMeasureToUnitOfMeasureDto implements Converter<UnitOfMeasure, UnitOfMeasureDto> {

    @Synchronized
    @Nullable
    @Override
    public UnitOfMeasureDto convert(UnitOfMeasure source) {
        if (source == null) {
            return null;
        }

        final UnitOfMeasureDto uomd = new UnitOfMeasureDto();
        uomd.setId(source.getId());
        uomd.setDescription(source.getDescription());
        return uomd;
    }
}
