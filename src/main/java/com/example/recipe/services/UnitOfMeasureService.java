package com.example.recipe.services;

import com.example.recipe.dtos.UnitOfMeasureDto;

import java.util.Set;

public interface UnitOfMeasureService {

    Set<UnitOfMeasureDto> listAllUoms();
}
