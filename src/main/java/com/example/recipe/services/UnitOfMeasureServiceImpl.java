package com.example.recipe.services;

import com.example.recipe.dtos.UnitOfMeasureDto;
import com.example.recipe.mappers.UnitOfMeasureToUnitOfMeasureDto;
import com.example.recipe.repositories.UnitOfMeasureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UnitOfMeasureServiceImpl implements UnitOfMeasureService {

    private final UnitOfMeasureRepository unitOfMeasureRepository;
    private final UnitOfMeasureToUnitOfMeasureDto toUnitOfMeasureDto;

    @Autowired
    public UnitOfMeasureServiceImpl(UnitOfMeasureRepository unitOfMeasureRepository, UnitOfMeasureToUnitOfMeasureDto toUnitOfMeasureDto) {
        this.unitOfMeasureRepository = unitOfMeasureRepository;
        this.toUnitOfMeasureDto = toUnitOfMeasureDto;
    }

    @Override
    public Set<UnitOfMeasureDto> listAllUoms() {
        return StreamSupport.stream(unitOfMeasureRepository.findAll()
                        .spliterator(), false)
                        .map(toUnitOfMeasureDto::convert)
                        .collect(Collectors.toSet());
    }
}
