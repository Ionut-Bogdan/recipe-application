package com.example.recipe.services;

import com.example.recipe.dtos.RecipeDto;
import com.example.recipe.mappers.RecipeDtoToRecipe;
import com.example.recipe.mappers.RecipeToRecipeDto;
import com.example.recipe.model.Recipe;
import com.example.recipe.repositories.RecipeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;
    private final RecipeDtoToRecipe toRecipe;
    private final RecipeToRecipeDto toRecipeDto;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository,
                             RecipeDtoToRecipe toRecipe, RecipeToRecipeDto toRecipeDto) {
        this.recipeRepository = recipeRepository;
        this.toRecipe = toRecipe;
        this.toRecipeDto = toRecipeDto;
    }

    @Override
    public Set<Recipe> getRecipes() {
        log.debug("I'm in the service");

        Set<Recipe> recipes = new HashSet<>();
        recipeRepository.findAll().iterator().forEachRemaining(recipes::add);
        return recipes;
    }

    @Override
    public Recipe findById(Long id) {
        Optional<Recipe> recipeOptional = recipeRepository.findById(id);

        if (recipeOptional.isEmpty()) {
            throw new RuntimeException("Recipe Not Found!");
        }
        return recipeOptional.get();
    }

    @Transactional
    @Override
    public RecipeDto findDtoById(Long id) {
        return toRecipeDto.convert(findById(id));
    }

    @Transactional
    @Override
    public RecipeDto saveRecipeDto(RecipeDto recipeDto) {
        Recipe detachedRecipe = toRecipe.convert(recipeDto);

        Recipe savedRecipe = recipeRepository.save(detachedRecipe);
        log.debug("Saved RecipeId: " + savedRecipe.getId());
        return toRecipeDto.convert(savedRecipe);
    }

    @Override
    public void deleteById(Long id) {
        recipeRepository.deleteById(id);
    }
}
