package com.example.recipe.services;

import com.example.recipe.dtos.IngredientDto;

public interface IngredientService {

    IngredientDto findByRecipeIdAndIngredientId(Long recipeId, Long ingredientId);

    IngredientDto saveIngredientDto(IngredientDto ingredientDto);
}
