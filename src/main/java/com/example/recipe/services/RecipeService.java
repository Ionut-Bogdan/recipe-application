package com.example.recipe.services;

import com.example.recipe.dtos.RecipeDto;
import com.example.recipe.model.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();

    Recipe findById(Long id);

    RecipeDto findDtoById(Long id);

    RecipeDto saveRecipeDto(RecipeDto recipeDto);

    void deleteById(Long id);
}
