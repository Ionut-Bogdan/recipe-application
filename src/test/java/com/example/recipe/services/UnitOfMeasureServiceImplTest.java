package com.example.recipe.services;

import com.example.recipe.dtos.UnitOfMeasureDto;
import com.example.recipe.mappers.UnitOfMeasureToUnitOfMeasureDto;
import com.example.recipe.model.UnitOfMeasure;
import com.example.recipe.repositories.UnitOfMeasureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class UnitOfMeasureServiceImplTest {

    UnitOfMeasureToUnitOfMeasureDto toUnitOfMeasureDto = new UnitOfMeasureToUnitOfMeasureDto();
    UnitOfMeasureService service;

    @Mock
    UnitOfMeasureRepository unitOfMeasureRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        service = new UnitOfMeasureServiceImpl(unitOfMeasureRepository, toUnitOfMeasureDto);
    }

    @Test
    void listAllUoms() {

        Set<UnitOfMeasure> unitOfMeasures = new HashSet<>();
        UnitOfMeasure uom1 = new UnitOfMeasure();
        uom1.setId(1L);
        unitOfMeasures.add(uom1);

        UnitOfMeasure uom2 = new UnitOfMeasure();
        uom1.setId(2L);
        unitOfMeasures.add(uom2);

        when(unitOfMeasureRepository.findAll()).thenReturn(unitOfMeasures);

        Set<UnitOfMeasureDto> dtos = service.listAllUoms();

        assertEquals(2, dtos.size());
        verify(unitOfMeasureRepository, times(1)).findAll();
    }
}