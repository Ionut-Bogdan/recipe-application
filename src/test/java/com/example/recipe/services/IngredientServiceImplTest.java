package com.example.recipe.services;

import com.example.recipe.dtos.IngredientDto;
import com.example.recipe.mappers.IngredientDtoToIngredient;
import com.example.recipe.mappers.IngredientToIngredientDto;
import com.example.recipe.mappers.UnitOfMeasureToUnitOfMeasureDto;
import com.example.recipe.model.Ingredient;
import com.example.recipe.model.Recipe;
import com.example.recipe.repositories.RecipeRepository;
import com.example.recipe.repositories.UnitOfMeasureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class IngredientServiceImplTest {

    private final IngredientToIngredientDto toIngredientDto;

    @Mock
    IngredientDtoToIngredient toIngredient;

    @Mock
    UnitOfMeasureRepository unitOfMeasureRepository;

    @Mock
    RecipeRepository recipeRepository;

    IngredientService ingredientService;

    public IngredientServiceImplTest() {
        this.toIngredientDto = new IngredientToIngredientDto(new UnitOfMeasureToUnitOfMeasureDto());
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        ingredientService = new IngredientServiceImpl(toIngredientDto, toIngredient, recipeRepository, unitOfMeasureRepository);
    }

    @Test
    void findByRecipeIdAndIngredientId() {

        Long recipeId = 1L;
        Long ingredient1Id = 1L;
        Long ingredient2Id = 2L;
        Long ingredient3Id = 3L;

        Recipe recipe = new Recipe();
        recipe.setId(recipeId);

        Ingredient ingredient1 = new Ingredient();
        ingredient1.setId(ingredient1Id);

        Ingredient ingredient2 = new Ingredient();
        ingredient1.setId(ingredient2Id);

        Ingredient ingredient3 = new Ingredient();
        ingredient1.setId(ingredient3Id);

        recipe.addIngredient(ingredient1);
        recipe.addIngredient(ingredient2);
        recipe.addIngredient(ingredient3);
        Optional<Recipe> recipeOptional = Optional.of(recipe);

        when(recipeRepository.findById(anyLong())).thenReturn(recipeOptional);

        IngredientDto ingredientDto = ingredientService.findByRecipeIdAndIngredientId(recipeId, ingredient3Id);

        assertEquals(ingredient3Id, ingredientDto.getId());
        assertEquals(recipeId, ingredientDto.getRecipeId());
        verify(recipeRepository, times(1)).findById(anyLong());
    }

    @Test
    void testSaveRecipeDto() {

        IngredientDto ingredientDto = new IngredientDto();
        ingredientDto.setId(3L);
        ingredientDto.setRecipeId(2L);

        Optional<Recipe> recipeOptional = Optional.of(new Recipe());

        Recipe savedRecipe = new Recipe();
        savedRecipe.addIngredient(new Ingredient());
        savedRecipe.getIngredients().iterator().next().setId(3L);

        when(recipeRepository.findById(anyLong())).thenReturn(recipeOptional);
        when(recipeRepository.save(any())).thenReturn(savedRecipe);

        IngredientDto savedDto = ingredientService.saveIngredientDto(ingredientDto);

        assertEquals(3, savedDto.getId());
        verify(recipeRepository, times(1)).findById(anyLong());
        verify(recipeRepository, times(1)).save(any(Recipe.class));
    }
}